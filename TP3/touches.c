#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>

#include "graphique.h"
#include "defines.h"
float trans_axeZ;
float angle_rotY;
float angle_rotX;
float kx;
float ky;
float kz;
void gererclavier(unsigned char touche, int x,int y)
{
	switch(touche)
	{
	case 43 : trans_axeZ -= 1.0; break;
	case 45 : trans_axeZ += 1.0; break;
	case '6' : angle_rotY += 10.0; break;
	case '4' : angle_rotY -= 10.0; break;
	case '8' : angle_rotX += 10.0; break;
	case '2' : angle_rotX -= 10.0; break;
	case 'h' : 
	if(helice_active == OFF)
	{
		helice_active = ON;
	}
	else
	{
		helice_active = OFF;
	}break;
	case 'r' : 
	if(roue_active == OFF)
	{
		roue_active = ON;
	}
	else
	{
		roue_active = OFF;
	}break;
	case 'a' : 
	if(avion_active == OFF)
	{
		avion_active = ON;
	}
	else
	{
		avion_active = OFF;
	}break;
	}
	glutPostRedisplay();
}
void gererClavierSpe(unsigned char touche, int x,int y)
{
    switch(touche)
    {
    case GLUT_KEY_RIGHT: kx*=2.0; break;
    case GLUT_KEY_LEFT: kx*=0.5; break;
	case GLUT_KEY_UP: ky*=2.0; break;
	case GLUT_KEY_DOWN: ky*=0.5; break;
    case GLUT_KEY_PAGE_UP: kz*=2.0; break;
    case GLUT_KEY_PAGE_DOWN: kz*=0.5; break;        
    }
    glutPostRedisplay();
}
