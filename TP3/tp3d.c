#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "touches.h"
#include "graphique.h"
#include "defines.h"

/* dimensions initiales de la fen�tre d'affichage */
#define LARGEUR  256
#define HAUTEUR  256
float trans_axeZ;
float angle_rotY;
float angle_rotX;
float angle_rotZ;
float kx;
float ky;
float kz;

/**
 * Fonction d'initialisation des param�tres d'affichage
 */
static void init_screen(void)
{
	if(angle_helice > 360)
	{
		angle_helice = 0.0;
	}
	if(angle_roue > 360)
	{
		angle_roue = 0.0;
	}
	if(angle_avion > 360)
	{
		angle_avion = 0.0;
	}
	trans_axeZ = -30.0;
	angle_rotY = 20.0;
	angle_rotX = 0.0;
	angle_rotZ = 0.0;
    kx = 1.0;
    kz = 1.0;
    ky = 1.0;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0,(float)(LARGEUR/HAUTEUR),1.0,250.0);
	/* par defaut toute la fen�tre est utilis� */
	glViewport(0,0,LARGEUR,HAUTEUR);
}


/**
 * Fonction principale qui cr�e et initialise la fen�tre
 * d'affichage et lance la boucle d'affichage Glut.
 * @param argc le nombre d'argument sur la ligne de commande
 * @param argv une table de cha�ne de caract�res contenant chacun
 * des arguments de la ligne de commande.
 */
int main (int argc, char *argv[])
{

  glutInit (&argc, argv);

  glutInitWindowPosition(100, 100); 
  glutInitWindowSize(LARGEUR, HAUTEUR); 

  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE);

  glutCreateWindow(/*argv[0]*/"TP2 made in Renaudin");

  /* choix de la fonction de rafraichissement */
  glutIdleFunc(animer);
  glutDisplayFunc(dessiner);
  glutReshapeFunc(retailler);

  init_screen();
  glutKeyboardFunc(gererclavier);
  glutSpecialUpFunc(gererClavierSpe);
  glEnable(GL_DEPTH_TEST);
  glutMainLoop();
  

  return 0;
}


