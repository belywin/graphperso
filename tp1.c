#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "graphique.h"
#include "clavier.h"
#include "souris.h"
#include "geometrie.h"

float taillePoint = 1.0;
float lineSize = 1.0;
point2D point_2D;

int main (int argc, char *argv[])
{
point_2D.x = UNDEFINED;	
point_2D.y = UNDEFINED;	
/* initialiser glut */
glutInit (&argc,argv);

/* specification de la taille de la fenetre */
glutInitWindowSize(256,256);

/* specification de la position de la fenetre */
glutInitWindowPosition(100,100);

/* creer la fenetre */
glutCreateWindow(argv[0]);

/* choix de la fonction de rafraichissement */
glutDisplayFunc(dessiner);

/* evenementielle clavier */
glutKeyboardFunc(gestionClavier);

/* evenementielle souris */
glutMouseFunc(gestionSouris);

/* evenementielle clavier spécial*/
glutSpecialUpFunc(gestionClavierFleche);

/* demarrer la boucle evenementielle */
glutMainLoop();
}
