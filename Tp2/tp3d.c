#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "touches.h"
#include "graphique.h"

/* dimensions initiales de la fen�tre d'affichage */
#define LARGEUR  256
#define HAUTEUR  256
float trans_axeZ;
float angle_rotY;
float angle_rotX;

/**
 * Fonction d'initialisation des param�tres d'affichage
 */
static void init_screen(void)
{
	trans_axeZ = -5.0;
	angle_rotY = 20.0;
	angle_rotX = 0.0;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0,(float)(LARGEUR/HAUTEUR),1.0,50.0);
	/* par defaut toute la fen�tre est utilis� */
	glViewport(0,0,LARGEUR,HAUTEUR);
}


/**
 * Fonction principale qui cr�e et initialise la fen�tre
 * d'affichage et lance la boucle d'affichage Glut.
 * @param argc le nombre d'argument sur la ligne de commande
 * @param argv une table de cha�ne de caract�res contenant chacun
 * des arguments de la ligne de commande.
 */
int main (int argc, char *argv[])
{

  glutInit (&argc, argv);

  glutInitWindowPosition(100, 100); 
  glutInitWindowSize(LARGEUR, HAUTEUR); 

  glutInitDisplayMode(GLUT_DEPTH);

  glutCreateWindow(argv[0]);

  /* choix de la fonction de rafraichissement */
  glutDisplayFunc(dessiner);
  glutReshapeFunc(retailler);

  init_screen();
  glutKeyboardFunc(gererclavier);
  glEnable(GL_DEPTH_TEST);
  glutMainLoop();
  

  return 0;
}


