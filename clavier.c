#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "globales.h"
void gestionClavier(unsigned char touche, int x, int y)
{
	switch(touche)
	{
		case 43 : taillePoint += 1.0; break;
		case 45 : if(taillePoint-1.0 >= 0) taillePoint -= 1.0; break;
	}
	glutPostRedisplay();
}
void gestionClavierFleche(unsigned char touche, int x, int y)
{
	switch(touche)
	{
		case GLUT_KEY_RIGHT: point_2D.x+=0.1; break;
		case GLUT_KEY_LEFT: point_2D.x-=0.1; break;
		case GLUT_KEY_UP: point_2D.y+=0.1; break;
		case GLUT_KEY_DOWN: point_2D.y-=0.1; break;
	}
	glutPostRedisplay();
}
