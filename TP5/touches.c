#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>

#include "graphique.h"
#include "defines.h"
float trans_axeZ;
float angle_rotY;
float angle_rotX;
float kx;
float ky;
float kz;
float specularite;
void gererclavier(unsigned char touche, int x,int y)
{
	GLint valeurS;
	switch(touche)
	{
	case 43 : trans_axeZ -= 1.0; break;
	case 45 : trans_axeZ += 1.0; break;
	case '6' : angle_rotY += 10.0; break;
	case '4' : angle_rotY -= 10.0; break;
	case '8' : angle_rotX += 10.0; break;
	case '2' : angle_rotX -= 10.0; break;
	case 'h' : 
	if(helice_active == OFF)
	{
		helice_active = ON;
	}
	else
	{
		helice_active = OFF;
	}break;
	case 'r' : 
	if(roue_active == OFF)
	{
		roue_active = ON;
	}
	else
	{
		roue_active = OFF;
	}break;
	case 'a' : 
	if(avion_active == OFF)
	{
		avion_active = ON;
	}
	else
	{
		avion_active = OFF;
	}break;
	case 'l' :
	
	glGetIntegerv(GL_SHADE_MODEL,&valeurS);
	if(valeurS == GL_SMOOTH)
	{
		glShadeModel(GL_FLAT);
	}
	if(valeurS == GL_FLAT)
	{
		glShadeModel(GL_SMOOTH);
	}break;
	case 'S' : 
	if(specularite <= 128.0)
	{ 
		specularite += 0.5;
	}
	if(specularite >= 128.0)
	{
		specularite == 128.0;
	}break;
	case 's' : 
	if(specularite >= 0.0)
	{ 
		specularite -= 0.5;
	}
	if(specularite <= 128.0)
	{
		specularite == 0.0;
	}break;
	}
	glutPostRedisplay();
}
void gererClavierSpe(unsigned char touche, int x,int y)
{
    switch(touche)
    {
    case GLUT_KEY_RIGHT: kx*=2.0; break;
    case GLUT_KEY_LEFT: kx*=0.5; break;
	case GLUT_KEY_UP: ky*=2.0; break;
	case GLUT_KEY_DOWN: ky*=0.5; break;
    case GLUT_KEY_PAGE_UP: kz*=2.0; break;
    case GLUT_KEY_PAGE_DOWN: kz*=0.5; break;        
    }
    glutPostRedisplay();
}
