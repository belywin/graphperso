#include <GL/gl.h>
#include <GL/glu.h>
#include <glut.h>
#include "touches.h"
#include <math.h>
#include "defines.h"

/** 
 * Fonction permettant de dessiner un cube centr� sur l'origine 
 * du rep�re de de taille dimxdimxdim.
 * @param dim la taille du c�t� du cube.
 */
float angle_helice = 0.0;
int helice_active = OFF;
float angle_roue = 0.0;
int roue_active = OFF;
float angle_avion = 0.0;
int avion_active = OFF;
float specularite = 12.5;
static void cube(float dim)
{
  glBegin(GL_QUADS);
  /* face avant rouge */
  glColor3f(1.0, 0.0, 0.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f( dim/2, -dim/2, dim/2);
  glVertex3f( dim/2,  dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, dim/2);

  /* face droite verte */
  glColor3f(0.0, 1.0, 0.0);
  glVertex3f(dim/2, -dim/2, dim/2);
  glVertex3f(dim/2, -dim/2, -dim/2);
  glVertex3f(dim/2,  dim/2, -dim/2);
  glVertex3f(dim/2,  dim/2, dim/2);

  /* face gauche jaune */
  glColor3f(1.0, 1.0, 0.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, dim/2);
  glVertex3f(-dim/2,  dim/2, -dim/2);
  glVertex3f(-dim/2, -dim/2, -dim/2);

  /* face arriere blanche */
  glColor3f(1.0, 1.0, 1.0);
  glVertex3f(-dim/2, -dim/2, -dim/2);
  glVertex3f(-dim/2,  dim/2, -dim/2);
  glVertex3f( dim/2,  dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, -dim/2);

  /* face superieure cyan */
  glColor3f(0.0, 1.0, 1.0);
  glVertex3f(-dim/2, dim/2, dim/2);
  glVertex3f( dim/2, dim/2, dim/2);
  glVertex3f( dim/2, dim/2, -dim/2);
  glVertex3f(-dim/2, dim/2, -dim/2);

  /* face inferieure magenta */
  glColor3f(1.0, 0.0, 1.0);
  glVertex3f(-dim/2, -dim/2, dim/2);
  glVertex3f(-dim/2, -dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, -dim/2);
  glVertex3f( dim/2, -dim/2, dim/2);

  glEnd();
} 



/** 
 * Fonction permettant de dessiner le rep�re du monde sous 
 * forme d'une croix 3D.
 * @param dim la taille de la ligne repr�sentant un demi-axe.
 */

static void repere(float dim)
{

  glBegin(GL_LINES);
 
  glColor3f(1.0, 1.0, 1.0);
  glVertex3f(-dim, 0.0, 0.0);
  glVertex3f( dim, 0.0, 0.0);
  glVertex3f(0.0,-dim, 0.0);
  glVertex3f(0.0, dim, 0.0);
  glVertex3f(0.0, 0.0, -dim);
  glVertex3f(0.0, 0.0,  dim);

  glEnd();
}

void cylindre(float r, float h, int nb){
int i;
float x,y;
//glColor3f(1.0, 1.0, 1.0);
glBegin(GL_POLYGON);
		for(i=0;i<=360;i+=360/nb)
		{
			x=r*cos((i*3.14)/180);
			y=r*sin((i*3.14)/180);
			glVertex3f(x,y,0);
		}
glEnd();
//glColor3f(1.0, 0.0, 1.0);
glBegin(GL_QUAD_STRIP);
		for(i=0;i<=360;i+=360/nb)
		{
			x=r*cos((i*3.14)/180);
			y=r*sin((i*3.14)/180);
			glVertex3f(x,y,0);
            glVertex3f(x,y,h);
		}
glEnd();
//glColor3f(1.0, 1.0, 0.0);
glBegin(GL_POLYGON);
		for(i=0;i<=360;i+=360/nb)
		{
			x=r*cos((i*3.14)/180);
			y=r*sin((i*3.14)/180);
            glVertex3f(x,y,h);
		}
glEnd(); 
}
void pale(void)
{
glMatrixMode(GL_MODELVIEW);
glBegin(GL_POLYGON);
 glVertex3f(1.0,0.0,0.0);
 glVertex3f(0.0,1.4,0.0);
 glVertex3f(-1.0,0.0,0.0);
 glVertex3f(0.0,-1.4,0.0);
glEnd();
glPushMatrix();
glRotatef(180.0,0.0,0.0,-1.0);
glTranslatef(0.0,2.8,0.0);
glBegin(GL_POLYGON);
 glVertex3f(1.0,0.0,0.0);
 glVertex3f(0.0,1.4,0.0);
 glVertex3f(-1.0,0.0,0.0);
 glVertex3f(0.0,-1.4,0.0);
glEnd();
glPopMatrix();
glPushMatrix();
glRotatef(90.0,0.0,0.0,-1.0);
glTranslatef(1.4,1.4,0.0);
glBegin(GL_POLYGON);
 glVertex3f(1.0,0.0,0.0);
 glVertex3f(0.0,1.4,0.0);
 glVertex3f(-1.0,0.0,0.0);
 glVertex3f(0.0,-1.4,0.0);
glEnd();
glPopMatrix();
glPushMatrix();
glRotatef(90.0,0.0,0.0,-1.0);
glTranslatef(1.4,-1.4,0.0);
glBegin(GL_POLYGON);
 glVertex3f(1.0,0.0,0.0);
 glVertex3f(0.0,1.4,0.0);
 glVertex3f(-1.0,0.0,0.0);
 glVertex3f(0.0,-1.4,0.0);
glEnd();
glPopMatrix();
}
void helice(float r, float h, int nb)
{
glutSolidCone(r,h*2,nb,nb);
glColor3f(0.0,1.0,0.0); 
glTranslatef(0.0,1.4,1.7*h);
pale();
}
void roue(float rayonRoue,float rayonPneu)
{
  glRotatef(angle_roue,0.0,0.0,-1.0); 
  glColor3f(0.0,1.0,1.0);  
  glMatrixMode(GL_MODELVIEW);  
  glutSolidTorus(rayonRoue,rayonPneu,100,100);
  glPushMatrix();
  glColor3f(0.0,0.0,1.0);
  glRotatef(90.0,0.0,1.0,0.0);
  glutSolidCone(rayonRoue/8,rayonRoue,100,100);
  glPopMatrix();
  glPushMatrix();
  glRotatef(90.0,0.0,-1.0,0.0);
  glutSolidCone(rayonRoue/8,rayonRoue,100,100);
  glPopMatrix();
  glPushMatrix();
  glRotatef(90.0,1.0,0.0,0.0);
  glutSolidCone(rayonRoue/8,rayonRoue,100,100);
  glPopMatrix();
  glPushMatrix();
  glRotatef(90.0,-1.0,0.0,0.0);
  glutSolidCone(rayonRoue/8,rayonRoue,100,100);
  glPopMatrix();
}
/**
 * Fonction utilis�e pour afficher le monde 3D � visualiser. Cette fonction
 * sera appel�e � chaque fois qu'un "rafraichissement" de la fen�tre
 * d'affichage sera n�cessaire.
 */
void fuselage(float r, float h, int nb)
{
 glColor3f(1.0,0.0,0.0);   
 glutSolidSphere(r,nb,nb);
 cylindre(r,h,nb);
 glTranslatef(0.0,0.0,h);
 glutSolidCone(r,h,nb,nb);
 glColor3f(1.0,1.0,0.0);
 glRotatef(angle_helice,0.0,0.0,1.0); 
 helice(r,-h,nb);
}
void roues(void)
{
  glPushMatrix();
	  glRotatef(90.0,0.0,1.0,0.0);
	  glTranslatef(-5.0,-5.0,5.0);
	  roue(1.5,3.0);
  glPopMatrix();
  glPushMatrix();
	  glRotatef(90.0,0.0,1.0,0.0);
	  glTranslatef(-5.0,-5.0,-5.0);
	  roue(1.5,3.0);
  glPopMatrix();
  glPushMatrix();
	  glRotatef(90.0,0.0,1.0,0.0);
	  glTranslatef(-5.0,-5.0,-5.0);
	  glColor3f(1.0,0.0,1.0);
	  cylindre(1.0,10,100);
  glPopMatrix();
} 
 
void aile(void)
{
  glScalef(2.0,0.3,10.0);
  glutSolidSphere(2.0,100,100);
}
void dessiner(void)
{
  /* effacer l'ecran */
  glClearColor(0.0, 0.0, 0.0, 1.0);

  /* raz de la fenetre avec la couleur de fond */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /* dessin des objets */
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  
  glTranslatef(0.0,0.0,trans_axeZ);
  glScalef(kx,ky,kz);
  glRotatef(angle_rotY,0.0,1.0,0.0);
  glRotatef(angle_rotX,1.0,0.0,0.0);
  /*//cube(2.0);
  //glutWireSphere(2.0,10,10);  
  //glutSolidCone(1.0,2.0,10,10);
  //glutSolidTorus(1.0,1.5,10,10);
  //glutSolidTeapot(1.2);
  //cylindre(1.5,1.5,3);
  glTranslatef(0.0,0.0,-15);
  glRotatef(angle_avion,0.0,1.0,0.0);
  glPushMatrix();
	fuselage(5,10,100);
  glPopMatrix();
  roues();
  glPushMatrix();
	  glRotatef(90.0,0.0,1.0,0.0);
	  glTranslatef(-5.0,3.0,0.0);
	  glColor3f(1.0,1.0,1.0);
	  aile();
  glPopMatrix();
  repere(2.0);*/
  laTeapot();
  glFlush();
  glutSwapBuffers();
  return;

}
void laTeapot(void)
{
	GLfloat Couleur[] = {0.2,0.2,0.2,1.0};
	glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,Couleur);
	GLfloat Speculaire[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT,GL_SPECULAR,Speculaire);
	
	glMaterialf(GL_FRONT,GL_SHININESS,specularite);
	glutSolidTeapot(2.0);
	glutPostRedisplay();
}
void retailler(GLsizei largeur, GLsizei hauteur)
{	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(largeur >= hauteur)
	{
		gluPerspective(60.0,(float)(hauteur/hauteur),1.0,250.0);
		glViewport((largeur-hauteur)/2,0,hauteur,hauteur);
	}
	else
	{
		gluPerspective(60.0,(float)(largeur/largeur),1.0,250.0);
		glViewport(0,(hauteur-largeur)/2,largeur,largeur);
	}		
	/*glViewport(0,0,50,50);*/
	
	glutPostRedisplay();
}
void animer(void)
{
	if (helice_active == ON)
	{
		angle_helice += 1.0;
	}
	if (roue_active == ON)
	{
		angle_roue += 1.0;
	}
	if (avion_active == ON)
	{
		angle_avion = 90.0;
		angle_avion += 1.0;
		angle_rotY += 1.0;

	}
	glutPostRedisplay();	
}
