#ifndef SRC_OBJET_HPP
#define SRC_OBJET_HPP
#include <iostream>
#include "Materiau.hpp"
#include "Vecteur.hpp"
#include "Rayon.hpp"
#include "Intersection.hpp"
class Objet{
    protected:
	Materiau matObj;
	Vecteur vectorObj;
	public:
	Objet(){
		matObj = Materiau();
	}
	virtual bool intersection(const Rayon& r, Intersection& inter) const = 0;
	virtual void Print() = 0;
};
#endif
