#ifndef SRC_VECTEUR_HPP
#define SRC_VECTEUR_HPP
#include <iostream>
#include <math.h>
class Vecteur{
	private :
	float dx;
	float dy;
	float dz;
	public :
	Vecteur(float pDx = 0.0, float pDy = 0.0, float pDz = 0.0){
		dx = pDx;
		dy = pDy;
		dz = pDz;
	}
	float getDx() const{
		return dx;
	}
	float getDy() const{
		return dy;
	}
	float getDz() const{
		return dz;
	}
	void setDx(int pDx){
		this -> dx = pDx;
	}
	void setDy(int pDy){
		this -> dy = pDy;
	}
	void setDz(int pDz){
		this -> dz = pDz;
	}
	 void Print() {
        std::cout << "(" << dx << "," << dy << "," << dz << ")";
    }
	 void normal(){
		 float normal = sqrt(pow(this -> dx,2)+pow(this -> dy,2)+pow(this -> dz,2));
		 this -> dx = this -> dx/normal;
		 this -> dy = this -> dy/normal;
		 this -> dz = this -> dz/normal;
	 }
};
#endif
