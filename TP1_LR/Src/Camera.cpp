#ifndef SRC_CAMERA_CPP
#define SRC_CAMERA_CPP
#include "Camera.hpp"
#include "Rayon.hpp"
#include "Vecteur.hpp"
#include "Intersection.hpp"
#include <iostream>
#include <string>
#include <math.h>
void Camera::genererImage(const Scene& sc, Image& im){
	//Rayon rayon(position,Vecteur vecteur(0.0,0.0,0.0));
	float lPix = 2.0/im.getLargeur();
	float hPix = 2.0/im.getHauteur();
	Couleur colorBl(1.0,1.0,1.0);
	Couleur colorB(0.0,0.0,1.0);
	for(int i=0; i < im.getHauteur(); i++){
		for(int j=0; j < im.getLargeur(); j++){
				float milPixX = ((j*2.0/im.getLargeur())-1.0) + lPix/2.0 ;
				float milPixY = (-(i*2.0/im.getHauteur())+1.0) - hPix/2.0;
						float normal = sqrt(pow(milPixX,2)+pow(milPixY,2)+pow(-2,2));
						float normalX = milPixX/normal;
						float normalY = milPixY/normal;
						float normalZ = -2/normal;
					Vecteur vector(milPixX,milPixY,-2.0);
					vector.normal();
					if(j==0 && i==0){
						std::cout << "pixel (" << i << "," << j <<") : rayon = ";
						position.Print();
						std::cout << "-> ";
						std::cout << "(" << vector.getDx()/*normalX milPixX*/ << "," << vector.getDy()/*normalY milPixY*/ << "," << vector.getDz()/*normalZ ",-2*/ <<")" << std::endl;
					}
					if(j==12 && i==7){
						std::cout << "pixel (" << i << "," << j <<") : rayon = ";
						position.Print();
						std::cout << "-> ";
						std::cout << "(" << vector.getDx()/*milPixX*/ << "," << vector.getDy()/*milPixY*/ << "," << vector.getDz()/*",-2*/ <<")" << std::endl;
					}	
					if(j==101 && i==200){
						std::cout << "pixel (" << i << "," << j <<") : rayon = ";
						position.Print();
						std::cout << "-> ";
						std::cout << "(" << vector.getDx()/*milPixX*/ << "," << vector.getDy()/*milPixY*/ << "," <<vector.getDz()/*",-2*/ <<")" << std::endl;
					}
				Rayon rayon(position,vector);
				Intersection pInter;
				if( sc.intersection(rayon,pInter) == true){
					im.setPixel(j,i,colorB);
				}
				else{
					im.setPixel(j,i,sc.getCol());
				}		
		}
	}	
}
#endif
