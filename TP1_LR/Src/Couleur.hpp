#ifndef SRC_COULEUR_HPP
#define SRC_COULEUR_HPP
#include <iostream>
class Couleur{
	public :
	float rouge;
	float vert;
	float bleu;
	Couleur(float r = 1.0, float v = 1.0, float b = 1.0){
		rouge = r;
		vert = v;
		bleu = b;
	}
	void Print(){
		std::cout << "( " << rouge << " , " << vert << " , " << bleu << " ) ";
	}
	int getRougei(){
		return rouge*255;
	}
	int getVerti(){
		return vert*255;
	}
	int getBleui(){
		return bleu*255;
	}
};
#endif
