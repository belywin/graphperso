#include <fstream>
#include "Scene.hpp"

Scene::Scene(std::string pTxt) {
    std::ifstream ifs(pTxt, std::ifstream::in);

    Materiau courant;

    while (!ifs.eof()) {
        std::string type;
        ifs >> type;

        if(type == "fond") {
            float r, v, b;
            ifs >> r >> v >> b;
            fond = Couleur(r, v, b);
        } else if (type == "materiau") {
            float aR, aV, aB, kd, ks, s;
            ifs >> aR >> aV >> aB >> kd >> ks >> s;
            courant = Materiau(Couleur(aR, aV, aB), kd, ks, s);
        } else if (type == "source") {
            float x, y, z, r, v, b;
            ifs >> x >> y >> z >> r >> v >> b;
            ensSour.push_back(new Sources(Intensite(r, v, b), Point(x, y, z)));
        } else if (type == "plan") {
            float a, b, c, d;
            ifs >> a >> b >> c >> d;
            ensObj.push_back(new Plan(courant, a, b, c, d));
        } else if (type == "sphere") {
            float x, y, z, r;
            ifs >> x >> y >> z >> r;
            ensObj.push_back(new Sphere(courant, Point(x, y, z), r));
        } else {
            std::string line;
            std::getline(ifs, line);
        }
    }
}

Scene::~Scene() {
    for (Objet* obj : ensObj)
        delete obj;
    ensObj.clear();
}

void Scene::Print() {
    std::cout << "Contenu de la scène :" << std::endl;
    
    for (Objet* obj : ensObj) {
        obj->Print();
        std::cout << std::endl;
    }
    
    for (Sources* src : ensSour) {
        src->Print();
        std::cout << std::endl;
    }
    std::cout << "La couleur de fond est ";
    fond.Print();
    std::cout << std::endl;
}
bool Scene::intersection(const Rayon& r, Intersection& inter) const{
		for (int i=0; i < ensObj.size(); i++){
			if (ensObj[i]->intersection(r,inter)){
				return true;
			}
		}
		return false;
	}
