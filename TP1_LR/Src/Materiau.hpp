#ifndef SRC_MATERIAU_HPP
#define SRC_MATERIAU_HPP
#include <iostream>
#include "Couleur.hpp"
class Materiau{
	private :
	Couleur matColor;
	float kd;
	float ks;
	float s;
	public :
	Materiau(Couleur pMatColor = Couleur(0.8,0.8,0.8), float pkd = 0.5, float pks = 0.1, float ps = 10){
		matColor = pMatColor;
		kd = pkd;
		ks = pks;
		s = ps;
	}
	Couleur getColor(){
		return matColor;
	}
	float getKd(){
		return kd;
	}
	float getKs(){
		return ks;
	}
	float getS(){
		return s;
	}
	void setColor(float pR, float pV, float pB){
		this -> matColor.rouge = pR;
		this -> matColor.bleu = pB;
		this -> matColor.vert = pV;
	}
	void setKd(float pKd){
		this -> kd = pKd;
	}
	void setKs(float pKs){
		this -> ks = pKs;
	}
	void setS(float pS){
		this -> s = pS;
	}
	void Print() {
		std::cout << "[ ";
		matColor.Print();
		std::cout << " , " << kd << " , " << ks << " , " << s << " ]";
	}
};
#endif
