#ifndef SRC_IMAGE_HPP
#define SRC_IMAGE_HPP
#include "Couleur.hpp"
class Image{
	private:
	Couleur *pixel;// tableau contenant les pixels de l'image
	int largeur, hauteur;// largeur et hauteur (en pixels) de l'image
	public:
	Image();
	Image(int largeur, int hauteur);
	~Image();
	int getLargeur();
	int getHauteur();
	void setPixel(int x, int y, Couleur c);
	Couleur getPixel(int x, int y);
	bool sauver(std::string filename);
};// Image
#endif
 
