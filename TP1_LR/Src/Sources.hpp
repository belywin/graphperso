#ifndef SRC_SOURCES_HPP
#define SRC_SOURCES_HPP
#include <iostream>
#include "Point.hpp"
#include "Intensite.hpp"
class Sources{
	private:
	Point position;
	Intensite Ilumi;
	public:
	Sources( Intensite pIlumi = Intensite(), Point pPosition = Point()){
		Ilumi = pIlumi;
		position = pPosition;
	}
	virtual void Print() {
        std::cout << "Source en ";
        position.Print();
        std::cout << " d'intensité ";
        Ilumi.Print();
    }
};
#endif
