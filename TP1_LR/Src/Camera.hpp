#ifndef SRC_CAMERA_HPP
#define SRC_CAMERA_HPP
#include "Scene.hpp"
#include "Image.hpp"
class Camera{
	private:
	Point position;
	public:
	Camera() : position(0.0,0.0,2.0){}
	void genererImage(const Scene& sc, Image& im);
};
#endif
