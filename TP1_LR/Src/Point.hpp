#ifndef SRC_POINT_HPP
#define SRC_POINT_HPP
#include <iostream>
class Point{
	private :
	float x;
	float y;
	float z;
	public :
	Point(float px = 0.0, float py = 0.0, float pz = 0.0){
		x = px;
		y = py;
		z = pz;
	}
	float getX() const{
		return x;
	}
	float getY() const{
		return y;
	}
	float getZ() const{
		return z;
	}
	void setX(int pX){
		this -> x = pX;
	}
	void setY(int pY){
		this -> y = pY;
	}
	void setZ(int pZ){
		this -> z = pZ;
	}
	void Print() {
		std::cout << " ( " << x << " , " << y << " , " << z << " ) ";
	}
};
#endif
