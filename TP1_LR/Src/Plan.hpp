#ifndef SRC_PLAN_HPP
#define SRC_PLAN_HPP
#include <iostream>
#include "Objet.hpp"
class Plan : public Objet{
	private:
	float a;
	float b;
	float c;
	float d;
	public:
	Plan(Materiau pMat = Materiau() , float pa = 0.0, float pb = 1.0, float pc = 0.0, float pd = 0.0){
		matObj = pMat;
		a = pa;
		b = pb;
		c = pc;
		d = pd;
	}
	bool intersection(const Rayon& r, Intersection& inter) const override{
		return false;
	}
	virtual void Print(){
		std::cout << "Plan : d'équation " << a << " x + " << b << " y + " << c << " z + " << d << " = 0 ";
		std::cout << " de materiau ";
		matObj.Print();
	}
};
#endif
