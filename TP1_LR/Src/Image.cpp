#ifndef SRC_IMAGE_CPP
#define SRC_IMAGE_CPP
#include "Image.hpp"
#include <string>
#include <fstream>
Image::Image(int largeur, int hauteur){
	this->largeur = largeur;
	this->hauteur = hauteur;
	pixel = new Couleur[largeur*hauteur];
}
Image::Image(){
	pixel = new Couleur[largeur*hauteur];
}
Image::~Image(){
	delete[] pixel;
}
int Image::getLargeur(){
	return largeur;
}
int Image::getHauteur(){
	return hauteur;
}
void Image::setPixel(int x, int y, Couleur c){
	int index = x+y*largeur;
	if (index < 0 || index >= largeur*hauteur){
		return;
	}
	else{
		pixel[x+y*largeur] = c;
	}
}
Couleur Image::getPixel(int x, int y){
	int index = x+y*largeur;
	if (index < 0 || index >= largeur*hauteur){
		return Couleur(0.0,0.0,0.0);
	}
	else{
		return pixel[x+y*largeur];
	}
}
bool Image::sauver(std::string filename){
	std::ofstream file(filename,std::ios::binary);
	file << "P3" << std::endl;
	file << largeur << " " << hauteur << std::endl;
	file << "255" << std::endl;
	for(int i = 0; i < largeur*hauteur; i++){
		file << pixel[i].getRougei() << " " << pixel[i].getVerti() << " " << pixel[i].getBleui() << std::endl;
	}
	return true;
}
#endif
