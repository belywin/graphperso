#ifndef SRC_SPHERE_HPP
#define SRC_SPHERE_HPP
#include <iostream>
#include "Couleur.hpp"
#include "Sphere.hpp"
#include "Plan.hpp"
#include "Sources.hpp"
#include "Rayon.hpp"
#include "Intersection.hpp"
#include "Objet.hpp"
#include <vector>
#include <string>

class Sphere : public Objet {
	private:
	Point centre;
	float rayon;
	const double PI = 3.1416;
	public:
	Sphere(Materiau pMat = Materiau(), Point pCentre = Point(), float pRayon = 1.0){
		matObj = pMat;
		centre = pCentre;
		rayon = pRayon;
	}
	bool intersection(const Rayon& r, Intersection& inter) const override;
	virtual void Print() {
        std::cout << "Sphere : de rayon " << rayon << ", de centre ";
        centre.Print();
        std::cout << " de matériau ";
        matObj.Print();
    }
};
#endif
