#ifndef SRC_RAYON_HPP
#define SRC_RAYON_HPP
#include "Point.hpp"
#include "Vecteur.hpp"
class Rayon{
	public:
	Point position;
	Vecteur direction;
	Rayon() : position(0.0,0.0,2.0),direction(0.0,0.0,0.0){}
	Rayon(Point pPos, Vecteur pDir){
		position = pPos;
		direction = pDir;
	}
};
#endif
