#ifndef SRC_SCENE_HPP
#define SRC_SCENE_HPP

#include "Couleur.hpp"
#include "Sphere.hpp"
#include "Plan.hpp"
#include "Sources.hpp"
#include "Rayon.hpp"
#include "Intersection.hpp"
#include "Objet.hpp"
#include <vector>
#include <string>

class Scene {
	private:
	std::vector<Objet*> ensObj;
	std::vector<Sources*> ensSour;
	Couleur fond;
	int i = 0;
    public:
    Scene(std::string pTxt);
	bool intersection(const Rayon& r, Intersection& inter) const;
	void fichierOuvr(std::string pTxt);
	Couleur getCol() const{return fond;} ;
	~Scene();
	void Print();
};
#endif
