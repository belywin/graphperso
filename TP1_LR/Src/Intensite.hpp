#ifndef SRC_INTENSITE_HPP
#define SRC_INTENSITE_HPP
#include <iostream>
class Intensite{
	private:
	float Ir;
	float Iv;
	float Ib;
	public:
	Intensite(float r = 1.0, float v = 1.0, float b = 1.0){
		Ir = r;
		Iv = v;
		Ib = b;
	}
	float getIr(){
		return Ir;
	}
	float getIv(){
		return Iv;
	}
	float getIb(){
		return Ib;
	}
	void setIr(float pIr){
		this -> Ir = pIr;
	}
	void setIv(float pIv){
		this -> Iv = pIv;
	}
	void setIb(float pIb){
		this -> Ib = pIb;
	}
	void Print(){
		std::cout << "( " << Ir << " , " << Iv << " , " << Ib << " ) ";
	}
};
#endif
