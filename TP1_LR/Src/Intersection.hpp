#ifndef INTERSECTION_HPP
#define INTERSECTION_HPP
#include "Point.hpp"

class Objet;

class Intersection : public Point{
	private:
	Objet *objet;
	float distance;
	public:
	Intersection();
	Intersection(const Point& p,Objet *o, const float& t);
	~Intersection();
};
#endif
