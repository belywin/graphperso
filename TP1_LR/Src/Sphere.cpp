#ifndef SPHERE_CPP
#define SPHERE_CPP
#include "Sphere.hpp"
#include <math.h>

bool Sphere::intersection(const Rayon& r, Intersection& inter) const{

		float ax = pow(r.direction.getDx(),2);
		float ay = pow(r.direction.getDy(),2);
		float az = pow(r.direction.getDz(),2);
		float a = ax+ay+az;

		float bx = (r.position.getX()-centre.getX())*(r.direction.getDx());
		float by = (r.position.getY()-centre.getY())*(r.direction.getDy());
		float bz = (r.position.getZ()-centre.getZ())*(r.direction.getDz());
		float b = 2*(bx+by+bz);

		float cx = pow((r.position.getX()-centre.getX()),2);
		float cy = pow((r.position.getY()-centre.getY()),2);
		float cz = pow((r.position.getZ()-centre.getZ()),2);
		float cr = pow(rayon,2);
		float c = cx+cy+cz-cr;
		
		float delta = pow(b,2)-4*a*c;
		
		if( delta < 0 ){
			return false;
		}
		else{
			return true;
		}	
}		
#endif
